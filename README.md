# trig_rate_estimation

# Rate estimator

Using the L1Topo rate estimation described at [this twiki]( https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RateEstimator).
The tool `menuFastTopo.py` itself is found on afs:

```
/afs/cern.ch/work/n/nakahama/public/HLTRates/
```
