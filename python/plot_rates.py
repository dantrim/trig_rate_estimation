#!/usr/bin/env python

import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True
r.gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

from optparse import OptionParser
import sys
import os

import glob

class EffPoint :
    def __init__(self, eff_type = "", threshold = "") :
        self._eff_type = eff_type
        self._threshold = threshold
        self._em_rate = 0.0 # Hz
        self._em_unique_rate = 0.0 # Hz
        self._mu_rate = 0.0 # Hz
        self._mu_unique_rate = 0.0 # Hz

    @property
    def eff_type(self) :
        return self._eff_type
    @property
    def threshold(self) :
        return self._threshold

    @property
    def em_rate(self) :
        return self._em_rate
    @em_rate.setter
    def em_rate(self, val) :
        self._em_rate = float(val)
    @property
    def em_unique_rate(self) :
        return self._em_unique_rate
    @em_unique_rate.setter
    def em_unique_rate(self, val) :
        self._em_unique_rate = float(val)

    @property
    def mu_rate(self) :
        return self._mu_rate
    @mu_rate.setter
    def mu_rate(self, val) :
        self._mu_rate = float(val)
    @property
    def mu_unique_rate(self) :
        return self._mu_unique_rate
    @mu_unique_rate.setter
    def mu_unique_rate(self, val) :
        self._mu_unique_rate = float(val)
    

def get_points(filelist) :

    eff_points = []
    for f in filelist :
        kin_type = ""
        if "DR" in f :
            kin_type = "DR"
        elif "DPHI" in f :
            kin_type = "DPHI"
        else :
            print "ERROR Unhandled kin type (or unfound)"
            sys.exit()

        threshold = f.split(kin_type)[1].replace(".txt","")
        threshold = float( "%s.%s" % (threshold[0], threshold[1]))

        pt = EffPoint(kin_type, threshold)

        lines = open(f).readlines()
        for iline, line in enumerate(lines) :
            if not line : continue
            if "+-" not in line : continue
            if kin_type not in line : continue
            line = line.strip()
            split_line = line.split("+-")
            data_rate_nom = float(split_line[-2].split()[-1])
            data_rate_err = float(split_line[-1].split()[-1])

            uniq_line = lines[iline+1]
            uniq_line = uniq_line.strip()
            uniq_rate = uniq_line.split("+-")
            uniq_rate_nom = float(uniq_rate[-2].split()[-1])
            uniq_rate_err = float(uniq_rate[-1].split()[-1])

            if "MU" in line :
                pt.mu_rate = data_rate_nom
                pt.mu_unique_rate = uniq_rate_nom
            elif "EM" in line :
                pt.em_rate = data_rate_nom
                pt.em_unique_rate = uniq_rate_nom

        eff_points.append(pt)

    return eff_points

def make_plots(filelist = []) :

    eff_points = get_points(filelist)

    kin_type = eff_points[0].eff_type

    bounds = {}
    bounds["DR"] = [0.2, 2.0, 5.0]
    bounds["DPHI"] = [0.1, 0.5, 2.2]

    n_bins = bounds[kin_type][2] - bounds[kin_type][1]
    n_bins = float(n_bins) / float(bounds[kin_type][0])
    n_bins = int(n_bins)

    histos = []
    c = r.TCanvas("c", "", 800, 600)
    c.cd()
    upper = r.TPad("upper", "upper", 0.0, 0.0, 1.0, 1.0)
    lower = r.TPad("lower", "lower", 0.0, 0.0, 1.0, 1.0)

    up_height = 0.75
    dn_height = 0.30
    upper.SetPad(0.0, 1.0 - up_height, 1.0, 1.0)
    lower.SetPad(0.0, 0.0, 1.0, dn_height)
    upper.SetTickx(0)
    upper.SetGridx(1)
    upper.SetGridy(1)
    lower.SetTickx(1)
    lower.SetGridx(1)
    lower.SetTicky(0)
    upper.SetRightMargin(0.05)
    lower.SetRightMargin(0.05)
    upper.SetLeftMargin(0.14)
    lower.SetLeftMargin(0.14)
    upper.SetTopMargin(0.7 * upper.GetTopMargin())
    upper.SetBottomMargin(0.09)
    lower.SetBottomMargin(0.4)
    upper.Draw()
    lower.Draw()
    c.Update()

    hm = r.TH1F("hm_%s" % kin_type, "", n_bins, bounds[kin_type][1], bounds[kin_type][2]) 
    hm.SetLineWidth(2)
    hm.SetLineColor(r.kRed)

    hmu = r.TH1F("hmu_%s" % kin_type, "", n_bins, bounds[kin_type][1], bounds[kin_type][2]) 
    hmu.SetLineWidth(2)
    hmu.SetLineColor(r.kRed)
    hmu.SetLineStyle(2)

    he = r.TH1F("he_%s" % kin_type, "", n_bins, bounds[kin_type][1], bounds[kin_type][2]) 
    he.SetLineWidth(2)
    he.SetLineColor(r.kBlue)

    heu = r.TH1F("heu_%s" % kin_type, "", n_bins, bounds[kin_type][1], bounds[kin_type][2]) 
    heu.SetLineWidth(2)
    heu.SetLineColor(r.kBlue)
    heu.SetLineStyle(2)

    thresholds = []
    for ep in eff_points :
        thresholds.append(ep.threshold)
    thresholds = sorted(thresholds)

    for ithresh, thresh in enumerate(thresholds) :
        ibin = ithresh + 1

        for eff in eff_points :
            if eff.threshold == thresh :
                hm.SetBinContent(ibin,eff.mu_rate)
                hmu.SetBinContent(ibin,eff.mu_unique_rate)
                he.SetBinContent(ibin,eff.em_rate)
                heu.SetBinContent(ibin,eff.em_unique_rate)
            else :
                continue

    leg_names = {}
    leg_names["hm_%s" % kin_type] = "MU Rate"
    leg_names["hmu_%s" % kin_type] = "Unique MU Rate"
    leg_names["he_%s" % kin_type] = "EM Rate"
    leg_names["heu_%s" % kin_type] = "Unique EM Rate"
    leg = r.TLegend(0.7, 0.7, 0.88, 0.88)
    leg.SetTextFont(42)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)

    upper.cd()

    histos = [he, heu, hm, hmu]
    maxy = -1
    miny = 99999999
    for h in histos :
        if h.GetMaximum() > maxy : maxy  = h.GetMaximum()
        if h.GetMinimum() < miny : miny = h.GetMinimum()
    maxy = 1.45 * maxy
    miny = 0 * miny
    for ih, h in enumerate(histos) :
        h.SetMaximum(maxy)
        h.SetMinimum(miny)
        xax = h.GetXaxis()
        yax = h.GetYaxis()

        xax.SetTitle(kin_type)
        xax.SetTitleFont(42)
        xax.SetLabelFont(42)
        xax.SetTitleOffset(999)
        xax.SetLabelOffset(999)

        yax.SetTitle("Rate [Hz]")
        yax.SetTitleFont(42)
        yax.SetLabelFont(42)

        leg.AddEntry(h, leg_names[h.GetName()], "l")

        if ih == 0 :
            h.Draw("hist")
        else :
            h.Draw("hist same")
        c.Update()

    leg.Draw()
    c.Update()

    # text
    text = r.TLatex()
    text.SetTextFont(42)
    text.SetTextSize(0.75 * text.GetTextSize())
    primary_triggers = "EM24VHI EM18VHI_3J20 MU10_2J20 MU20"
    mu_trig = "MU Topo: MU11_MU10_2J12_X"
    text.DrawLatexNDC(0.17, 0.88, mu_trig)
    el_trig = "EM Topo: EM20VHI_3J12_X"
    text.DrawLatexNDC(0.17, 0.84, el_trig)
    prim_str = "Unique | (%s)" % primary_triggers
    text.DrawLatexNDC(0.17, 0.80, prim_str)
    upper.Update()

    lower.cd()

    her = heu.Clone("he_ratio")
    her.Divide(he)
    hmr = hmu.Clone("hm_ratio")
    hmr.Divide(hm)

    hrs = [her, hmr]
    for ih, h in enumerate(hrs) :
        h.SetMaximum(0.7)
        h.SetMinimum(0)
        xax = h.GetXaxis()
        yax = h.GetYaxis()

        xax.SetTitle(kin_type)
        xax.SetTitleFont(42)
        xax.SetLabelFont(42)
        xax.SetTitleOffset(1)
        xax.SetTitleSize(4 * xax.GetTitleSize())
        xax.SetLabelSize(2.5 * xax.GetLabelSize())
        xax.SetLabelOffset(0.03)

        yax.SetTitle("#frac{Unique}{Raw}")
        yax.SetTitleFont(42)
        yax.SetLabelFont(42)
        yax.SetTitleSize(3 * yax.GetTitleSize())
        yax.SetTitleOffset(0.4) 
        yax.SetLabelSize(2.3 * yax.GetLabelSize())

        if ih == 0 :
            h.Draw("hist")
        else :
            h.Draw("hist same")
        

    #heu.Draw("hist")
    #hmu.Draw("hist same")
    c.Update()

    c.SaveAs("./rate_plots/topo_rates_%s.pdf" % kin_type)

def main() :

    parser = OptionParser()
    parser.add_option("-i", "--input", default="", help = "input dir contianing text files generated by menuFastTopo.py")
    (options, args) = parser.parse_args()
    input_dir = options.input

    files = glob.glob("%s/rate_estimate*.txt" % input_dir)
    n_files = len(files)
    print 50 * '-'
    print "n files found : %d" % n_files
    dr_files = [f for f in files if "DR" in f]
    dphi_files = [f for f in files if "DPHI" in f]
    print "n DR files    : %d" % len(dr_files)
    print "n DPHI files  : %d" % len(dphi_files)
    print 50 *  '-'

    if len(dr_files) > 0 :
        make_plots(dr_files)
    if len(dphi_files) > 0 :
        make_plots(dphi_files)

    

    

    
    

if __name__ == "__main__" :
    main()
