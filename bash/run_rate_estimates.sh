#!/bin/bash

default_dr_values="2.0" # 2.2 2.4 2.6 2.8 3.0 3.2 3.4 3.6 3.8 4.0 4.2 4.4 4.6 4.8 5.0"
default_dphi_values="2.0" #2.2 2.1 2.0 1.9 1.8 1.7 1.6 1.5 1.4 1.3 1.2 1.1 1.0 0.9 0.8 0.7 0.6 0.5"
primaries="EM24VHI EM18VHI_3J20 MU10_2J20 MU20"
output_dir=${PWD}

function print_usage {

    echo "----------------------------------------"
    echo " run_rate_estimates" 
    echo ""
    echo " usage: source <path-to>/run_rate_estimates.sh [options]"
    echo " options:"
    echo "  --dr            do only the DR L1Topo trigger estimates [default: BOTH DR and DPHI]"
    echo "  --dphi          do only the DPHI L1Topo trigger estimates [default: BOTH DR and DPHI]"
    echo "  --dr-values     set DR thresholds (space separated list)"
    echo "     > defaults: $default_dr_values"
    echo "  --dphi-values   set DPHI thresholds (space separated list)"
    echo "     > defaults: $default_dphi_values"
    echo "  --skip-root     don't setup the ROOT version (5.34)"
    echo "  -h|--help       print this help message"
    echo "----------------------------------------"
    
}

function run_dr_estimate {

    echo "============================================================="
    echo " RUNNING DR ESTIMATES : $1"
    echo "============================================================="

    for dr in $1; do
        drcut=${dr/.}
        #echo $drcut
        python menuFastTopo.py MU11_MU10_2J12:${drcut}DR-MU10-J12 EM18VHI_3J12:${drcut}DR-EM18VHI-J12 $primaries 2>&1 |tee ${output_dir}/rate_estimate_MU11_MU10_EM18VHI_DR${drcut}.txt
    done

}

function run_dphi_estimate {

    echo "============================================================="
    echo " RUNNING DPHI ESTIMATES : $1"
    echo "============================================================="

    for dphi in $1; do
        dphicut=${dphi/.}
        python menuFastTopo.py MU11_MU10_2J12:DPHI${dphicut}-MU10-J12 EM18VHI_3J12:DPHI${dphicut}-EM18VHI-J12 $primaries 2>&1 |tee ${output_dir}/rate_estimate_MU11_MU10_EM18VHI_DPHI${dphicut}.txt
    done

}


function main {

    do_dr="1"
    do_dphi="1"
    dr_values=$default_dr_values
    dphi_values=$default_dphi_values
    skip_root="0"

    while test $# -gt 0
    do
        case $1 in
            --dr)
                do_dphi="0"
                ;;
            --dphi)
                do_dr="0"
                ;;
            --dr-values)
                dr_values=${2}
                shift
                ;;
            --dphi-values)
                dphi_values=${2}
                shift
                ;;
            --skip-root)
                skip_root="1"
                ;;
            -h)
                print_usage
                return 0
                ;;
            --help)
                print_usage
                return 0
                ;;
            *)
                echo "ERROR Invalid argument: $1"
                return 1
                ;;
        esac
        shift
    done

    if [[ $skip_root -eq "0" ]]; then
        lsetup "root 5.34.25-x86_64-slc6-gcc48-opt"
    fi


    echo " * run_rate_estimates * "
    echo " do DR        : $do_dr"
    echo " do DPHI      : $do_dphi"
    echo " DR values    : $dr_values"
    echo " DPHI values  : $dphi_values"
    echo " output dir   : $output_dir"

    if [[ ! -d "./src/HLTRates/" ]]; then
        echo "ERROR src/HLTRates dir not found"
        return 1
    fi

    pushd ./src/HLTRates/

    if [[ $do_dr -eq "1" ]]; then
        run_dr_estimate "$dr_values"
    fi

    if [[ $do_dphi -eq "1" ]]; then
        run_dphi_estimate "$dphi_values"
    fi

    popd

}


#_______________
main $*
